package com.example.assingment2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    ArrayList<String> mobileArray= new ArrayList<String>() {{
        add("A");
        add("B");
        add("C");
    }};
    private Button addItem;
    private EditText getItem;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final ArrayAdapter adapter = new ArrayAdapter(this,
                R.layout.listview_layout, mobileArray);
        ListView listView =  findViewById(R.id.lv_customList);
        addItem = findViewById(R.id.butt_addItem);
        getItem = findViewById(R.id.et_getItem);
        listView.setAdapter(adapter);
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                mobileArray.remove(i);
                adapter.notifyDataSetChanged();
                return false;
            }
        });
        addItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(getItem.getText().toString().isEmpty()){
                    Toast.makeText(MainActivity.this, "INVALID!", Toast.LENGTH_SHORT).show();
                }
                else{
                    mobileArray.add(0,getItem.getText().toString());
                    adapter.notifyDataSetChanged();
                }
            }
        });
    }
}
